import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

// import { configureStore } from '@reduxjs/toolkit';
import reducer from './reducers/index';

// const initialState = {};

const middleware = [thunk];


// const store = configureStore({
//   reducer: { rootReducer }, initialState, applyMiddleware: { ...middleware }
// })

const store = createStore(
  reducer,
  // initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);


//store usage
export const unsubscribe = store.subscribe(() => {

  return store.getState()
}
);


export default store;
