import {
  GET_TECHS,
  ADD_TECH,
  TECHS_ERROR,
  SET_LOADING,
} from './types';


const dataDB = new URL('http://localhost');
dataDB.port = 8080
dataDB.pathname = 'techs'

// Get all techs from database
export const getTechs = () => async dispatch => {


  try {
    setLoading();
    const res = await fetch(dataDB.href);
    const data = await res.json();

    // console.log(data);

    dispatch({
      type: GET_TECHS,
      payload: data,
    });
    } catch (err) {
    dispatch({
      type: TECHS_ERROR,
      payload: err.response?.statusText,
    });
  }
};

// Add tech to the database
export const addTech = tech => async dispatch => {
  try {
    setLoading();

    const res = await fetch(dataDB.href, {
      method: 'POST',
      body: JSON.stringify(tech),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    const data = await res.json();
    // console.log(data);

    dispatch({
      type: ADD_TECH,
      payload: data,
    });

  } catch (err) {
    dispatch({
      type: TECHS_ERROR,
      payload: err.response?.statusText,
    });
  }
};

// Set loading true
export const setLoading = () => {
  return {
    type: SET_LOADING,
  };
};
