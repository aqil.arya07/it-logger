import React, { useEffect, useReducer, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addLog } from '../../actions/logActions';
import TechSelectOptions from '../techs/TechSelectOptions';
import M from 'materialize-css/dist/js/materialize.min.js';
// import { getTechs } from '../../actions/techActions';


//reducer function
const addLogReducer = (state, action) => {
  switch (action.type) {
    case 'setMessage': {
      return {
        ...state, message: action.payload
      }
    }
    case 'setTechnician': {
      return {
        ...state, tech: action.payload//store.getState()?.log?.currentTech
      }
    }
    case 'setPriority': {
      return {
        ...state, attention: !state.attention//store.getState()?.log?.currentTech
      }
    }

    default: {
      return { state }
    }
  }
}


const AddLogModal = ({ addLog }) => {

  //references
  const messageField = useRef();
  const attentionBox = useRef();

  //reducer initial value
  const initialValue = {
    message: '',
    tech: '',
    attention: false
  }




  const [state, dispatcher] = useReducer(addLogReducer, initialValue);

  //refactor to single line;
  const { message, tech, attention } = state;


  useEffect(() => {
    // 
    // console.log(selectTag.current);
    const selectTags = document.querySelector('select');
    // console.log(selectTags);
    selectTags.addEventListener('change', function (e) {

      dispatcher({
        type: 'setTechnician',
        payload: e.target.value
      })
    })
  }, [tech])


  const onSubmit = async () => {
    if (message === '' || tech === '') {
      M.toast({ html: 'Please enter a message and select a technician' });
    } else {
      const newLog = {
        message,
        attention,
        tech,
        date: new Date(),
      };
      addLog(newLog)
      // unsubscribe();
      M.toast({ html: `Log added by ${tech}` });

      // Clear fields
      attentionBox.current.checked = false;

      messageField.current.value = '';
    }
  };

  return (
    <div id='add-log-modal' className='modal' style={modalStyle}>
      <div className='modal-content'>
        <h4>Enter System Log</h4>
        <div className='row'>
          <div className='input-field'>

            <input
              ref={messageField}
              type='text'
              name='message'
              placeholder='Log Message'
              id='logMessage'
              onChange={(e) => {
                dispatcher({
                  type: 'setMessage',
                  payload: e.target.value
                })
              }}
            />
            <label htmlFor='message' className='active'>
              Log Message
            </label>
          </div>
        </div>

        <div className='row'>

          <p>Assigned Technician: {tech}</p>
          <div className='input-field'>
            <TechSelectOptions handleChange={(e) => {
              dispatcher({
                type: 'setTechnician',
                payload: e.target.value
              })
            }} />
          </div>
        </div>


        <div className='row'>
          <div className='input-field'>
            <p>
              <label>
                <input
                  ref={attentionBox}
                  type='checkbox'
                  className='filled-in'
                  checked={attention}
                  value={attention}
                  onChange={e => {
                    dispatcher({
                      type: 'setPriority'
                    })
                  }}
                />
                <span>Needs Attention</span>
              </label>
            </p>
          </div>
        </div>
      </div>
      <div className='modal-footer'>
        <a
          href='#!'
          onClick={onSubmit}
          id='submit'
          className='modal-close waves-effect blue waves-light btn'
        >
          Enter
        </a>
      </div>
    </div>
  );
};

AddLogModal.propTypes = {
  addLog: PropTypes.func.isRequired,
};

const modalStyle = {
  width: '75%',
  height: '75%',
};

export default connect(null, { addLog })(AddLogModal);
