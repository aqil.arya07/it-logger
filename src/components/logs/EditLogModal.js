import React, { useEffect, useReducer, useRef } from 'react';
import TechSelectOptions from '../techs/TechSelectOptions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updateLog } from '../../actions/logActions';
import M from 'materialize-css/dist/js/materialize.min.js';



//reducer function
const editLogReducer = (state, action) => {
  switch (action.type) {
    case 'setMessage': {
      return {
        ...state, message: action.payload
      }
    }
    case 'setTechnician': {
      return {
        ...state, tech: action.payload//store.getState()?.log?.currentTech
      }
    }
    case 'setPriority': {
      return {
        ...state, attention: action.payload//store.getState()?.log?.currentTech
      }
    }
    default: {
      return { state }
    }
  }
}

const EditLogModal = ({ current, updateLog }) => {
  // Component level state

  const inputCheckbox = useRef();
  //reducer initial value
  const initialValue = {
    message: '',
    tech: '',
    attention: false
  }


  const [state, dispatcher] = useReducer(editLogReducer, initialValue);


  //refactor to single line;
  const { message, tech, attention } = state;




  //useEffect to apply on initial load 
  //and when current element changes
  useEffect(() => {
    if (current !== null) {
      dispatcher({
        type: 'setMessage',
        payload: current.message
      })
      dispatcher({
        type: 'setTechnician',
        payload: current?.tech
      })
      dispatcher({
        type: 'setPriority',
        payload: current?.attention
      })

      inputCheckbox.current.checked = current?.attention

    }
    //eslint-disable-next-line
  }, [current]);


  const onSubmit = () => {

    if (message === '' || tech === '') {
      M.toast({ html: 'Please enter a message and tech' });
    } else {
      const updatedLog = {
        id: current.id,
        message,
        tech,
        attention,
        date: new Date(),
      };
      // console.log({ updatedLog });

      updateLog(updatedLog)
      // unsubscribe();


      M.toast({ html: `Log updated by ${tech}` });

      // Clear fields
      // 5. Clear all input fields after save
    }
  };
  return (
    <div id='edit-log-modal' className='modal' style={modalStyle}>
      <div className='modal-content'>
        <h4>Edit System Log</h4>
        <div className='row'>
          <div className='input-field'>
            <input
              type='text'
              name='message'
              value={message}
              onChange={e => dispatcher({
                type: 'setMessage',
                payload: e.target.value
              })}
            />
          </div>
        </div>

        <div className='row'>
          <div className='input-field'>
            <p>Assigned Technician: {current?.tech}</p>
            <TechSelectOptions handleChange={(e) => {

              //if the current technician is not matching the selected technician
              //then apply local dispatch update
              if (current?.tech !== e.target.value) {
                dispatcher({
                  type: 'setTechnician',
                  payload: e.target.value
                })

              }

            }} />

          </div>
        </div>

        <div className='row'>
          <div className='input-field'>
            <p>
              <span>Please press <kbd>Space</kbd> to Toggle Checkbox.
                &nbsp;</span>
              <label>
                <input
                  ref={inputCheckbox}
                  type='checkbox'
                  className='filled-in'
                  defaultChecked={current?.attention}

                  onClick={e => {
                    dispatcher({
                      type: 'setPriority',
                      payload: e.target.checked//current?.attention
                    })
                  }
                  }
                  onChange={e => {
                    dispatcher({
                      type: 'setPriority',
                      payload: e.target.checked//current?.attention
                    })
                  }
                  }

                />
                <span>Needs Attention</span>
              </label>
            </p>
          </div>
        </div>
      </div>
      <div className='modal-footer'>
        <a
          href='#!'
          onClick={onSubmit}
          className='modal-close waves-effect blue waves-light btn'
        >
          Enter
        </a>
      </div>
    </div>
  );
};

const modalStyle = {
  width: '75%',
  height: '75%',
};

EditLogModal.propTypes = {
  current: PropTypes.object,
  updateLog: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  current: state.log.current,
});

export default connect(mapStateToProps, { updateLog })(EditLogModal);
