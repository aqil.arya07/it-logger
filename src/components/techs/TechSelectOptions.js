import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { unsubscribe } from '../../store';
import { getTechs } from '../../actions/techActions';


const TechSelectOptions = ({ tech, handleChange, getTechs, current }) => {

  //references for the select tag and initial option tag
  const selectTag = useRef();
  const defaultOption = useRef();


  useEffect(() => {
    let isLoading = tech.loading;
    if (isLoading === false) {
      getTechs();
    }

    return () => {

      isLoading = true;
      unsubscribe();

    }

  }, [tech.loading, getTechs])

  //technician options to display 
  const renderedOptions = tech.techs?.map(techie => {

   
    return (
      <option key={techie?.id} id={techie?.id} value={`${techie?.firstName}, ${techie?.lastName}`}>{techie?.firstName}, {techie?.lastName}</option>//{techie?.firstName}, {techie?.lastName}

    )
  })



  return (
    <div >
      <select onChange={(e) => {
        handleChange(e)

      }
      } ref={selectTag} className='browser-default' id='techieList' name='techies'>
        <option hidden ref={defaultOption} key='defaultValue'>{current?.tech}</option>
        {
          renderedOptions
        }
      </select>
    </div>
  )
};


//required props
TechSelectOptions.propTypes = {
  tech: PropTypes.object.isRequired,
  getTechs: PropTypes.func.isRequired,

  current: PropTypes.object,
};

//maps the state to the props
//using the connect feature below
const mapStateToProps = state => ({
  tech: state.tech,

  current: state.log.current,
});

// export default TechSelectOptions

export default connect(mapStateToProps, { getTechs })(TechSelectOptions);
