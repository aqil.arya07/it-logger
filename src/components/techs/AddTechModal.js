import React, { useReducer, useRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addTech } from '../../actions/techActions';
import M from 'materialize-css/dist/js/materialize.min.js';

//reducer function
const technicianReducer = (state, dispatch) => {
  switch (dispatch.type) {
    case 'handleFirstName': {
      return {
        ...state, firstName: dispatch.payload
      }
    }
    case 'handleLastName': {
      return {
        ...state, lastName: dispatch.payload
      }
    }
    default: {
      return { state }
    }
  }
}


const AddTechModal = ({ addTech }) => {

  const firstName = '';
  const lastName = '';

  //reducer initial value
  const initialValue = {
    firstName: firstName,
    lastName: lastName
  }

  //reducer function  to handle state updates
  const [state, dispatch] = useReducer(technicianReducer, initialValue);


//references for first and last name of technician
  const firstname = useRef(state.firstName);
  const lastname = useRef(state.lastName);


  const onSubmit = async () => {
    if (state.firstName === '' || state.lastName === '') {

      M.toast({ html: 'Please enter the first and last name' });
    } else {
      
      //Use the `addTech` redux action provided to save the new technician to the db.json "database".
      addTech(state)
      M.toast({ html: `${state.firstName} ${state.lastName} was added as a tech` });

      //Clear input fields after save
      firstname.current.value = '';
      lastname.current.value = '';
    }

  };

  return (
    <div id='add-tech-modal' className='modal'>
      <div className='modal-content'>
        <h4>New Technician</h4>
        <input ref={firstname} placeholder='First Name' id='f_name' onChange={(e) => {
          dispatch({
            type: 'handleFirstName',
            payload: e.target.value
          })
        }} />
        <input ref={lastname} placeholder='Last Name' id='l_name' onChange={(e) => {
          dispatch({
            type: 'handleLastName',
            payload: e.target.value
          })
        }} />
      </div>
      <div className='modal-footer'>
        <a
          href='#!'
          onClick={onSubmit}
          className='modal-close waves-effect blue waves-light btn'
        >
          Enter
        </a>
      </div>
    </div>
  );
};

AddTechModal.propTypes = {
  addTech: PropTypes.func.isRequired,
};

export default connect(null, { addTech })(AddTechModal);
